/label ~bug ~"priority: medium"

### One-sentence description

<!-- What breaks -->

### Current behaviour / Reproducing the bug

<!-- Please write what is happening and how we could reproduce it, if relevant -->

<!--
1. Step 1
2. Step 2
3. ???
4. Breakage
-->

### Expected behaviour

<!-- Please write how what happened did not meet your expectations -->
