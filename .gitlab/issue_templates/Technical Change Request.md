/label ~"technical change" ~"priority: low"

<!--
    This template is for changes that do not affect the behaviour of the website.

    ** If you are not in the Technicie, there is a very high chance that you
       should not use this template

    Examples:

     * Changes in CI
     * Refactoring of code
     * Technicie-facing documentation
-->

### One-sentence description

<!-- Please provide a brief description of the issue. Don't go into specifics. -->

### Why?

<!-- Please motivate why we should invest into this change -->

### Current implementation

<!-- If relevant, describe how it's done currently -->

### Suggested implementation

<!-- Provide (a) suggestion(s) for how we could approach this -->
